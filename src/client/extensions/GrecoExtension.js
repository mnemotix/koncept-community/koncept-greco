import {Extension} from "@mnemotix/koncept-core";
import {WelcomeMessage} from "../components/Home/WelcomeMessage";
import {ConceptRelationsPrepend} from "../components/Concept/ConceptRelationsPrepend";
import {ConceptPropertiesPrepend} from "../components/Concept/ConceptPropertiesPrepend";
import {ConceptListItemBadge} from "../components/Concept/ConceptListItemBadge";
import {extendConceptActions} from "../components/Concept/extendConceptActions";
import {extendSchemeActions} from "../components/Scheme/extendSchemeActions";

export const grecoExtension = new Extension({
  name: "GrecoExtension",
  generateTopRoutes: ({isContributor} = {}) => <></>,
  fragments: {
    HomeWelcomeMessage: WelcomeMessage,
    ConceptRelationsPrepend: ConceptRelationsPrepend,
    ConceptPropertiesPrepend: ConceptPropertiesPrepend,
    ConceptListItemBadge: ConceptListItemBadge
  },
  settings: {
    ConceptActions: {
      extendConceptActions: extendConceptActions
    },
    SchemeActions: {
      extendSchemeActions: extendSchemeActions
    }
  }
});
