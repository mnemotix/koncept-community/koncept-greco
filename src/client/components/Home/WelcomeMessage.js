import {Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import logo from "../../assets/logo-region-sud.png";

const useStyles = makeStyles((theme) => ({
  logo: {
    display: "block",
    margin: theme.spacing(12, "auto")
  }
}));

/**
 *
 */
export function WelcomeMessage({} = {}) {
  const classes = useStyles();
  const {t} = useTranslation();

  return (
    <>
      <img src={logo} width={"auto"} height={300} alt={"Logo de la région Sud"} className={classes.logo} />
    </>
  );
}
