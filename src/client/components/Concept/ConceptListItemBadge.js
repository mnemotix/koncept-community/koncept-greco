import {useTranslation} from "react-i18next";
import {Chip, Avatar, Tooltip} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    marginRight: theme.spacing(0.5)
  },
  label: {
    paddingRight: 0
  }
}));
/**
 *
 */
export function ConceptListItemBadge({concept} = {}) {
  const classes = useStyles();
  const {t} = useTranslation();

  return (
    <Choose>
      <When condition={(concept?.types || []).includes("Skill")}>
        {renderChip({avatarLetter: t("SKILL.LABEL").charAt(0).toUpperCase(), tooltip: t("SKILL.TOOLTIP")})}
      </When>
      <When condition={(concept?.types || []).includes("Occupation")}>
        {renderChip({avatarLetter: t("OCCUPATION.LABEL").charAt(0).toUpperCase(), tooltip: t("OCCUPATION.TOOLTIP")})}
      </When>
    </Choose>
  );

  function renderChip({avatarLetter, tooltip}) {
    return (
      <Tooltip title={tooltip}>
        <Chip avatar={<Avatar>{avatarLetter}</Avatar>} color="secondary" classes={classes} size="small" />
      </Tooltip>
    );
  }
}
