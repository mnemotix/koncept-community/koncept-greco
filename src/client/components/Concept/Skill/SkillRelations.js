import {memo} from "react";
import {pickedPropsAreEquals, InfiniteScrollColumn} from "@mnemotix/koncept-core";
import {Typography, ListItem, ListItemText, Tooltip} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";

import {gqlSkillsRelations} from "./gql/SkillRelations.gql";

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2)
  },
  empty: {
    color: theme.palette.text.emptyHint
  },
  propertyItem: {
    marginTop: theme.spacing(3)
  },
  relations: {
    borderLeft: `1px solid ${theme.palette.grey[300]}`,
    paddingLeft: theme.spacing(4)
  }
}));

/**
 *
 */
const SkillRelations = memo(({id, onClickAssociatedConcept} = {}) => {
  const classes = useStyles();
  const {t} = useTranslation();

  const {data: {skill} = {}, loading} = useQuery(gqlSkillsRelations, {
    variables: {id}
  });

  return (
    <div className={classes.root}>
      <Tooltip title={t("SKILL.OCCUPATIONS.TOOLTIP")} placement="left">
        <Typography variant={"h6"} gutterBottom>
          {t("SKILL.OCCUPATIONS.TITLE")}
        </Typography>
      </Tooltip>
      <Choose>
        <When condition={loading || skill?.occupationsCount > 0}>
          <InfiniteScrollColumn
            dense={true}
            hasNextPage={false}
            loading={loading}
            onLoadNextPage={() => {}}
            totalCount={skill?.occupationsCount || 5}>
            {(skill?.occupations?.edges || []).map(({node: occupation}) => (
              <ListItem key={occupation.id} button onClick={() => onClickAssociatedConcept({concept: occupation})}>
                <ListItemText primary={occupation.prefLabel} secondary={occupation.schemePrefLabel} />
              </ListItem>
            ))}
          </InfiniteScrollColumn>
        </When>
        <Otherwise>
          <span className={classes.empty}>{t("CONCEPT.EMPTY_RELATIONSHIP")}</span>
        </Otherwise>
      </Choose>
    </div>
  );
}, pickedPropsAreEquals(["skill.id"]));

export default SkillRelations;
