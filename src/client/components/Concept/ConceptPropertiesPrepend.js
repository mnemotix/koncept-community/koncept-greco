import {Chip, Avatar} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";

const useStyles = makeStyles((theme) => ({
  chip: {
    marginTop: theme.spacing(2)
  }
}));

/**
 *
 */
export function ConceptPropertiesPrepend({concept} = {}) {
  const classes = useStyles();
  const {t} = useTranslation();

  return (
    <Choose>
      <When condition={(concept?.types || []).includes("Skill")}>
        <Chip
          avatar={<Avatar>{t("SKILL.LABEL").charAt(0).toUpperCase()}</Avatar>}
          label={t("SKILL.CHIP_LABEL")}
          color="secondary"
          className={classes.chip}
        />
      </When>
      <When condition={(concept?.types || []).includes("Occupation")}>
        <Chip
          avatar={<Avatar>{t("OCCUPATION.LABEL").charAt(0).toUpperCase()}</Avatar>}
          label={t("OCCUPATION.CHIP_LABEL")}
          color="secondary"
          className={classes.chip}
        />
      </When>
    </Choose>
  );
}
