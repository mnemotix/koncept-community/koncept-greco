import {useState} from "react";
import {Typography, Tooltip} from "@material-ui/core";
import {ToggleButtonGroup, ToggleButton} from "@material-ui/lab";
import {makeStyles} from "@material-ui/core/styles";
import {ConceptCreate as ConceptCreateBase, ConceptRelationEdit} from "@mnemotix/koncept-core";
import {useTranslation} from "react-i18next";
import {gqlCreateSkill} from "./Skill/gql/CreateSkill.gql";
import {gqlCreateOccupation} from "./Occupation/gql/CreateOccupation.gql";
import {skillOccupationsLinkInputDefinition} from "./Skill/form/Skill.form";
import {occupationSkillsLinkInputDefinition} from "./Occupation/form/Occupation.form";

const useStyles = makeStyles((theme) => ({
  toggleButton: {
    marginBottom: theme.spacing(2),
    width: "100%",
    display: "flex",

    "& > button": {
      flex: 1
    }
  }
}));

/**
 * @param {object} vocabulary
 * @param {object} [scheme]
 * @param {object} [broaderConcept]
 * @param {boolean} [topInScheme=false]
 * @returns {JSX.Element}
 * @constructor
 */
export function ConceptCreate({vocabulary, scheme, broaderConcept, topInScheme = false} = {}) {
  const classes = useStyles();
  const {t} = useTranslation();
  const [conceptType, setConceptType] = useState("basic");

  const gqlCreateMutations = {
    skill: gqlCreateSkill,
    occupation: gqlCreateOccupation
  };

  return (
    <>
      <Typography variant={"h6"} gutterBottom>
        {t("CONCEPT.TYPE")}
      </Typography>
      <ToggleButtonGroup
        size="small"
        value={conceptType}
        exclusive
        onChange={(_, destination) => {
          setConceptType(destination);
        }}
        className={classes.toggleButton}>
        <ToggleButton value="basic">{t("CONCEPT.LABEL")}</ToggleButton>
        <ToggleButton value="skill" color="secondary">
          {t("SKILL.LABEL")}
        </ToggleButton>
        <ToggleButton value="occupation" color="secondary">
          {t("OCCUPATION.LABEL")}
        </ToggleButton>
      </ToggleButtonGroup>
      <ConceptCreateBase
        vocabulary={vocabulary}
        scheme={scheme}
        broaderConcept={broaderConcept}
        topInScheme={topInScheme}
        gqlCreateConcept={gqlCreateMutations[conceptType]}>
        <If condition={conceptType === "skill"}>
          <Tooltip title={t("SKILL.OCCUPATIONS.TOOLTIP")} placement="left">
            <Typography variant={"h6"} gutterBottom>
              {t("SKILL.OCCUPATIONS.TITLE")}
            </Typography>
          </Tooltip>
          <ConceptRelationEdit linkInputDefinition={skillOccupationsLinkInputDefinition} vocabulary={vocabulary} />
        </If>
        <If condition={conceptType === "occupation"}>
          <Tooltip title={t("OCCUPATION.SKILLS.TOOLTIP")} placement="left">
            <Typography variant={"h6"} gutterBottom>
              {t("OCCUPATION.SKILLS.TITLE")}
            </Typography>
          </Tooltip>
          <ConceptRelationEdit linkInputDefinition={occupationSkillsLinkInputDefinition} vocabulary={vocabulary} />
        </If>
      </ConceptCreateBase>
    </>
  );
}
