import {useTranslation} from "react-i18next";
import SkillRelations from "./Skill/SkillRelations";
import OccupationRelations from "./Occupation/OccupationRelations";

/**
 *
 */
export function ConceptRelationsPrepend({concept, onClickAssociatedConcept} = {}) {
  const {t} = useTranslation();

  return (
    <Choose>
      <When condition={(concept?.types || []).includes("Skill")}>
        <SkillRelations id={concept.id} onClickAssociatedConcept={onClickAssociatedConcept} />
      </When>
      <When condition={(concept?.types || []).includes("Occupation")}>
        <OccupationRelations id={concept.id} onClickAssociatedConcept={onClickAssociatedConcept} />
      </When>
    </Choose>
  );
}
