import {memo} from "react";
import {pickedPropsAreEquals, InfiniteScrollColumn} from "@mnemotix/koncept-core";
import {Typography, ListItem, ListItemText, Tooltip} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";

import {gqlOccupationsRelations} from "./gql/OccupationRelations.gql";

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2)
  },
  empty: {
    color: theme.palette.text.emptyHint
  },
  propertyItem: {
    marginTop: theme.spacing(3)
  },
  relations: {
    borderLeft: `1px solid ${theme.palette.grey[300]}`,
    paddingLeft: theme.spacing(4)
  }
}));

/**
 *
 */
const OccupationRelations = memo(({id, onClickAssociatedConcept} = {}) => {
  const classes = useStyles();
  const {t} = useTranslation();

  const {data: {occupation} = {}, loading} = useQuery(gqlOccupationsRelations, {
    variables: {id}
  });

  return (
    <div className={classes.root}>
      <Tooltip title={t("OCCUPATION.SKILLS.TOOLTIP")} placement="left">
        <Typography variant={"h6"} gutterBottom>
          {t("OCCUPATION.SKILLS.TITLE")}
        </Typography>
      </Tooltip>
      <Choose>
        <When condition={loading || occupation?.skillsCount > 0}>
          <InfiniteScrollColumn
            dense={true}
            hasNextPage={false}
            loading={loading}
            onLoadNextPage={() => {}}
            totalCount={occupation?.skillsCount || 5}>
            {(occupation?.skills?.edges || []).map(({node: occupation}) => (
              <ListItem key={occupation.id} button onClick={() => onClickAssociatedConcept({concept: occupation})}>
                <ListItemText primary={occupation.prefLabel} secondary={occupation.schemePrefLabel} />
              </ListItem>
            ))}
          </InfiniteScrollColumn>
        </When>
        <Otherwise>
          <span className={classes.empty}>{t("CONCEPT.EMPTY_RELATIONSHIP")}</span>
        </Otherwise>
      </Choose>
    </div>
  );
}, pickedPropsAreEquals(["occupation.id"]));

export default OccupationRelations;
