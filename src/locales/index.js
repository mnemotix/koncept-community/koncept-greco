import merge from "lodash/merge";
import { fr } from "./fr";
import { en } from "./en";

import { locales as coreLocales } from "@mnemotix/koncept-core/locales"

export const locales = {
  fr: merge(coreLocales.fr, fr),
  en:  merge(coreLocales.en, en)
};

