export const fr = {
  HOME: {
    TITLE: "Référentiels de compétences",
    WELCOME_PHRASE: "Bienvenue sur GRECO",
  },
  SKILL: {
    LABEL: "Compétence",
    CHIP_LABEL: "Concept compétence",
    TOOLTIP: "Ce concept spécifie une compétence",
    OCCUPATIONS: {
      TITLE: "Métiers liés",
      TOOLTIP: "Liste des métiers en relation avec cette compétence",
    },
  },
  OCCUPATION: {
    LABEL: "Métier",
    TOOLTIP: "Ce concept spécifie un métier",
    CHIP_LABEL: "Concept Métier",
    SKILLS: {
      TITLE: "Compétences liées",
      TOOLTIP: "Liste des compétences en relation avec ce métier",
    },
  },
};
