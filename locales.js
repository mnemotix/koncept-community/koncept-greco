/**
 *
 *
 * Shortcut file, in order to be able to write in client code :
 *
 *    import [...] from '@mnemotix/koncept-greco/locales'
 *
 */

module.exports = require('./lib/locales');
